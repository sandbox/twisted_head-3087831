<?php

/**
 * @file
 * Connects Drupal with the irl server.
 *
 * This file contains most of the functions.
 *
 * @category user_management
 * @package irl
 */

/**
 * Checks if the user is protected via irl 2FA.
 */
function irl_user_protected_verify($uid) {

  $sql = "SELECT * FROM {irl_users} WHERE idUser = :uid";

  $result = db_query($sql, array(':uid' => $uid));

  $protected = [
    'hasKey' => FALSE,
    '2faEnabled' => FALSE,
  ];

  if ($result) {
    while ($row = $result->fetchAssoc()) {
      $protected['hasKey'] = $row['hasKey'] == 'y' ? TRUE : FALSE;
      $protected['2faEnabled'] = $row['2faEnabled'] == 'y' ? TRUE : FALSE;
    }
  }

  return $protected;
}

/**
 * Manage my irl.
 *
 * A few links and status of your account.
 * TODO: template this.
 */
function irl_manage($uid) {

  global $user;

  $content = '<h2>' . t('Manage your irl.') . '</h2>';

  // Are you protected?
  $protect = irl_user_protected_verify($user->uid);

  $content .= '<p>';
  if ($protect['hasKey']) {
    $content .= t('You have a key set in your account.');
    $content .= '<br />' . l(t('View my key'), "irl/manage/viewkey", array(
      'attributes' => array(
        'title' => t("View my key"),
      ),
    )) . '<br />';

  }
  else {
    $content .= t('You DO NOT have a key set in your account.') . '<br />';
    $content .= l(t('Add a key to my account'), "irl/manage/addKey", array(
      'attributes' => array(
        'title' => t("Add a key to my account"),
      ),
    )) . '<br />';

  }
  $content .= '</p>';

  $content .= '<p>';
  if ($protect['2faEnabled']) {
    $content .= t('You have 2FA enabled.') . '<br />';
    $content .= l(t('Disable 2FA with irl from your account.'), "irl/2fa/disable", array(
      'attributes' => array(
        'title' => t("Disable protection via 2fa."),
      ),
    )) . '<br />';

  }
  else {
    $content .= t('You DO NOT have 2FA enabled with irl.') . '<br />';
    $content .= l(t('Enable 2FA with irl'), "irl/2fa/enable", array(
      'attributes' => array(
        'title' => t("Enable 2FA to protect my account."),
      ),
    )) . '<br />';
  }

  $content .= '</p>';
  /*
  $content .= '<br />' . l(t('View my logs'), "irl/manage/viewlogs", array(
  'attributes' => array(
  'title' => t("View my logs"),
  ),
  ));
   */
  return $content;
}

/**
 * Form to request user email or username.
 */
function irl_login_form($form, &$form_state) {

  $form['userName'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 60,
    '#required' => TRUE,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Generate new challenge'),
  );

  return $form;
}

/**
 * Validate the form: login information.
 */
function irl_login_form_validate($form, &$form_state) {

  $user = user_load_by_name($form['userName']['#value']);

  if (!$user) {
    form_set_error('userName', t('Incorrect username'));
  }
  else {
    $form_state['values']['userName'] = $user->mail;
  }

  return TRUE;
}

/**
 * Submit the form: login information.
 */
function irl_login_form_submit($form, &$form_state) {

  drupal_goto('user/irl/' . $form_state['values']['userName']);

}

/**
 * Login via irl, complete form with challenge.
 */
function irl_login_finish_form($form, &$form_state) {

  global $base_url, $user;

  $theUser = user_load_by_mail(arg(2));

  if (!$theUser) {
    drupal_set_message(t('Unable to locate user.', 'error'));
    drupal_goto('user/irl');
  }

  // Are you protected?
  $p = irl_user_protected_verify($theUser->uid);

  if (!$p['2faEnabled']) {

    drupal_set_message(t('Your account is NOT protected with 2FA via irl, you need to enable this future to secure your account.'), 'error');
    // BUG: This is just not working.
    drupal_goto('user/irl');
    return FALSE;
  }

  // print_r($form_state);
  if (isset($form_state['input']['accessCode'])) {
    $accessCode = $form_state['input']['accessCode'];
    $challenge = $form_state['input']['challenge'];
  }
  else {

    $data = array(
      'w' => 'irl_challenge_me',
      'uid' => $theUser->uid,
    );

    $challenge = irl_connect_server($data);

    if (isset($challenge['err'])) {
      drupal_set_message(t('There was an error requesting your login, please try again later or contact the administrator.'), 'error');
      watchdog(
      'irl',
      'Unable to get a new challenge, response was: %err Request was: %req',
      array(
        '%err' => $challenge['err'],
        '%req' => json_encode($data),
      ),
      WATCHDOG_ERROR);

      drupal_goto('<front>');

    }
    $accessCode = $challenge['resp']->accessCode;
    $challenge = json_encode($challenge);
  }

  $clientId = variable_get('irl_server_client_id', '1');
  $secretToken = variable_get('irl_server_token', '');

  $serverPath = irl_get_server_path();

  $js = '';
  $js .= "var irl_base_url = '$base_url'; \n";
  $js .= "var irl_base_server_url = '$serverPath'; \n";
  $js .= "var irl_uid = '$theUser->uid'; \n";
  $js .= "var irl_clientId = '$clientId'; \n";
  $js .= "jQuery(document).ready(function(){ \n";
  $js .= "irl_successChallengeMe('" . $challenge . "');";
  $js .= '});';

  drupal_add_js($js, 'inline');
  drupal_add_js(drupal_get_path('module', 'irl') . '/irl.js');
  drupal_add_js(drupal_get_path('module', 'irl') . '/qr/qrcode.min.js');

  $content = '<p>' . t('Scan this code into your irl app in order to login.') . '</p>';
  $content .= '<div id="irlLoginQR"></div>';
  $content .= '<div>' . t('You may copy/paste this url in your irl app.');
  $content .= '<br /><input type="text" id="irlLoginLink" size="35"></div>';

  $form['userName'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#size' => 60,
    '#value' => arg(2),
    '#required' => TRUE,
    '#disabled' => TRUE,
  );

  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => TRUE,
  );

  $form['qrcode'] = array(
    '#type' => 'item',
    '#markup' => $content,
  );

  $form['challenge'] = array(
    '#type' => 'hidden',
    '#title' => t('Challenge'),
    '#size' => 60,
    '#default_value' => $challenge,
    '#required' => TRUE,
    '#attributes' => array('style' => 'display: none;'),
  );

  $form['accessCode'] = array(
    '#type' => 'hidden',
    '#title' => t('Access code'),
    '#size' => 60,
    '#default_value' => $accessCode,
    '#required' => TRUE,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Log in'),
  );

  return $form;

}

/**
 * Validate the form: finish login.
 *
 * TODO: I do not control flood/abuse here, but the challenge verification
 * is done before the password, so, this is a very slow process. Maybe
 * that will prevent some abbuse. Maybe I will add the flood.
 * The flood process is quite complicated, maybe I will just change all this
 * and 'alter' the regular login form.
 */
function irl_login_finish_form_validate($form, &$form_state) {

  $u = user_load_by_mail($form_state['values']['userName']);

  if (!$u) {
    form_set_error('title', t('User does not exist.'));
  }

  // Confirm that the challenge was passed.
  $p = _irl_check_for_passed_test($u->uid, $form_state['input']['accessCode']);

  if (!$p) {
    form_set_error('title', t('Challenge failed.'));
  }
  else {
    // All good, lets validate the password and login.
    if (user_authenticate($u->name, $form_state['values']['password'])) {
      _irl_login_user($u->uid);
    }
    else {
      form_set_error('title', t('Wrong password.'));
    }
  }

  return TRUE;
}

/**
 * Creates a log in page.
 */
function irl_log_in_page() {

  global $user, $base_url;

  // If user is logged in, I should leave!
  if ($user->uid > 0) {
    drupal_goto('<front>');
  }

  // If no user is indicated, present a form to get it.
  if (arg(2) == '') {

    $content = drupal_get_form('irl_login_form');
    return $content;

  }

  // $theUser = user_load_by_mail(arg(2));
  $c = drupal_get_form('irl_login_finish_form');
  return $c;

}

/**
 * I check if a test was passed in ajax requests.
 *
 * I reply 1 if passed and 0 if not.
 */
function irl_check_for_passed_test() {

  $test = _irl_check_for_passed_test(arg(3), arg(2));

  if ($test->resp == 1) {
    // Proceed to login.
    _irl_login_user(arg(3));
    print 1;
    return TRUE;
  }

  print 0;

}

/**
 * Helper function to actually check if a test was passed.
 */
function _irl_check_for_passed_test($uid, $accessCode) {

  global $base_url, $user;

  $data = array(
    'w' => 'irl_challenge_confirm',
    'uid' => $uid,
    'accessCode' => $accessCode,
  );

  $test = irl_connect_server($data);

  if ($test && @$test['resp'] == 1) {
    return TRUE;
  }

  return FALSE;

}

/**
 * Logs in a user if the challenge was passed.
 *
 * Helper function.
 */
function _irl_login_user($uid) {
  global $user;
  $user = user_load($uid);
  user_login_finalize();
  return TRUE;
}

/**
 * View the key in my account.
 */
function irl_key_view() {

  global $user, $base_url;

  $content = t('This is your current key.');

  $key = irl_key_request($user->uid);

  if (isset($key['err'])) {
    drupal_set_message(t('Unable to locate a key for you, maybe set one up? !here', array('!here' => l(t('here'), 'irl/manage/addkey'))), 'error');
    $key = '';
  }
  else {
    $key = $key['resp']->pubKey;
  }

  $content = '<p>' . t('This is your public key. You may confirm that it is the same that you already have.') . '</p>';
  $content .= '<textarea style="width: 100%" rows="5" id="irl_pubKey">' . $key . '</textarea>';

  return $content;
}

/**
 * Request a key from the irl server.
 */
function irl_key_request($uid) {

  $key = irl_connect_server(
        array(
          'w' => 'irl_retrieve_key',
          'uid' => $uid,
        )
  );

  return $key;

}

/**
 * Add a new key to my account.
 */
function irl_key_add_new() {

  global $user, $base_url;

  drupal_add_js(drupal_get_path('module', 'irl') . '/qr/qrcode.min.js');

  $key = irl_connect_server(
        array(
          'w' => 'irl_open_key_access',
          'uid' => $user->uid,
        )
  );

  // Precaution in case there is no response.
  // @todo handle this better, maybe something in the error log
  // or watch something log
  // and I am not sure if this is the actual error, it could be
  // already set up or // something else.
  if (isset($key['err'])) {
    drupal_set_message(t('You already have a key set up. Maybe you are trying to update it?'), 'error');
    drupal_goto('irl/manage/viewkey');
    // drupal_goto('<front>');.
    return FALSE;
  }

  $key = $key['resp'];

  $url = sprintf('//irl/?task=keyRegisterNew&uid=%s&secret=%s&url=%s&clientId=%s',
        $user->uid, $key->secretCode, irl_get_server_path(), variable_get('irl_server_client_id', '1'));

  $js = '
  jQuery(document).ready(function(){
	// Generate QR Code
	 path = \'' . $url . '\';
  var qrcode = new QRCode(document.getElementById("irlQRCode"),{width: 250,height: 250});
  qrcode.makeCode(path);
jQuery("#irlUrlNewKey").val(path);
  });
  ';

  drupal_add_js($js, 'inline');

  $content = t('You will add a new key here.') . '<br /><br />';
  $content .= t('Once you are finished you may !url.', array('!url' => l(t('see your key here'), 'irl/manage/viewKey'))) . ' <br /><br />';
  $content .= '<div id="irlQRCode"></div>';
  $content .= '<br />' . t('You may copy this address and put it in your irl app') . '<br />';
  $content .= '<input type="text" size="20" id="irlUrlNewKey">';

  return $content;
}

/**
 * Creates a registration page.
 */
function irl_register_page() {

  global $user, $base_url;

  drupal_add_js(drupal_get_path('module', 'irl') . '/qr/qrcode.min.js');

  $serverPath = variable_get('irl_server_path', IRL_E2GO_SERVER);

  $path = "$base_url/?q=irl/register/process";

  $url = "//irl/?task=userRegister&url=$path&serverPath=$serverPath";
  // $url = "//irl/?t=ur&u=$path&serverPath=$serverPath";
  $js = "
  jQuery(document).ready(function(){;
// Generate QR Code
	 //path = '$url';
  var qrcode = new QRCode(document.getElementById('irlQrRegister'),{width: 250,height: 250});
  qrcode.makeCode('$url');
jQuery('#irlUrlRegister').val('$url');
  });";

  drupal_add_js($js, 'inline');

  $content = '<div id="irlQrRegister"></div>';
  $content .= '<br />' . t('You may copy this address and put it in your irl app') . '<br />';
  $content .= '<input type="text" size="20" id="irlUrlRegister">';

  return $content;

}

/**
 * Do the actual registration of a new user.
 */
function irl_register_process() {

  global $base_url;

  // Check that the user name is available.
  if ($_POST['userName'] == '' || !isset($_POST['userName'])) {
    print json_encode(array('error' => 'noUserName'));
    exit;
  }

  $user = user_load_by_name($_POST['userName']);
  if ($user) {
    print json_encode(array('error' => 'userNameExists'));
    exit;
  }

  // Check for a key.
  if ($_POST['pubKey'] == '' || !isset($_POST['pubKey'])) {
    print json_encode(array('error' => 'noPubKey'));
    exit;
  }

  // An email may be optional, if not required and not provided
  // I will just make one up.
  $emailRequire = variable_get('irl_require_email_for_registration', 1);

  if ($_POST['email'] == '' || !isset($_POST['email'])) {

    if ($emailRequire) {
      print json_encode(array('error' => 'email required'));
      exit;
    }
    else {
      $_POST['email'] = rand(0, 1000) . time() . '@example.com';
    }
  }
  else {
    // Check that the email is not registered.
    $user = user_load_by_mail($_POST['email']);
    if ($user) {
      print json_encode(array('error' => 'emailExists'));
      exit;
    }
  }

  // All good, lets register a user and add the key.
  // This will generate a random password, you could set your own here.
  $password = user_password(8);

  // Set up the user fields.
  $fields = array(
    'name' => $_POST['userName'],
    'mail' => $_POST['email'],
    'pass' => $password,
    'status' => 1,
    'init' => 'email address',
    'roles' => array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
    ),
  );

  // The first parameter is left blank so a new user is created.
  $account = user_save('', $fields);

  // If you want to send the welcome email, use the following code.
  // Manually set the password so it appears in the e-mail.
  $account->password = $fields['pass'];

  // Open a space for the key.
  $keyReg = irl_connect_server(
        array(
          'w' => 'irl_open_key_access',
          'uid' => $account->uid,
          'pubKey' => $_POST['pubKey'],
        )
  );

  // @todo if the key registration fails, delete user or something
  if (isset($keyReg['err'])) {
    print json_encode(array('error' => 'failedToRegisterKey', 'r' => json_encode($keyReg)));
    exit;
  }

  // Whatchdog.
  watchdog('irl', 'New user registration');

  print json_encode(array('resp' => 'ALL_GOOD'));

}

/**
 * Checks if the response from the irl server is an error.
 *
 * Takes a json and returns a parsed object.
 * TODO: add 404 responses and the rest.
 */
function irl_check_resp_error($r) {

  $r = json_decode($r);

  // Old system.
  if (isset($r->resp) && !is_object($r->resp)) {
    if ($r->resp < 0) {
      return array('err' => $r->resp);
    }
  }

  // New system.
  if (isset($r->err)) {
    return array('err' => $r->err);
  }
  // No output at all!
  // This also happens if the response is not a valid JSON
  // if there are php errors in the server this will happen.
  if ($r == '') {
    return array('err' => 'Literally an empty page, maybe that url does not exist or is dead! or just malformed json in the response.' . $r);
  }

  return (array) $r;

}

/**
 * Get the path to the irl server.
 */
function irl_get_server_path() {

  global $base_url;

  return variable_get('irl_server_path', IRL_E2GO_SERVER);

}

/**
 * Connects to the remote server.
 */
function irl_connect_server($data, $url = FALSE) {

  global $base_url;

  // Add the client Id and pwd to all calls.
  // Except if it was already provided for some reason.
  // @todo add pwd
  $data['secretToken'] = variable_get('irl_server_token', '0');
  $data['clientId'] = isset($data['clientId']) ? $data['clientId'] : variable_get('irl_server_client_id', '1');

  $url = $url ? $url : variable_get('irl_server_path', IRL_E2GO_SERVER);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POST, TRUE);

  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  $output = curl_exec($ch);
  $info = curl_getinfo($ch);
  curl_close($ch);

  // Error with the server path.
  if ($info['http_code'] == '301') {
    watchdog(
    'irl',
    'Unable to connect to a remote server: %server',
    array('%server' => $url . '+' . json_encode($data)),
    WATCHDOG_ERROR);
    return array('err' => 'ERR_301');
  }

  // Check for errors.
  $output = irl_check_resp_error($output);

  // Error with the request.
  if (isset($output['err'])) {
    watchdog(
    'irl',
    'Request returned an error: %server. Data: %data. Err: %err',
    array(
      '%server' => $url,
      '%data' => json_encode($data),
      '%err' => $output['err'],
    ),
    WATCHDOG_ERROR);
  }

  return $output;

}

/**
 * View logs.
 */
function irl_log_view($uid = 0) {

  global $user;

  $ini = arg(3) ? arg(3) : 0;

  // If no user is indicated I will assume it is for 'me'.
  if ($uid == 0) {
    $uid = $user->uid;
  }

  $logs = irl_connect_server(array(
    'w' => 'irl_logs_get',
    'uid' => $uid,
    'ini' => $ini,
  ));

  if (isset($logs['err'])) {
    $content = t('No logs available at this moment.');
    return $content;
  }

  // Back button.
  $back = '';
  if ($ini > 0) {
    $back = l(t('<<'), 'irl/manage/viewlogs/' . ($ini - 10 < 0 ? 0 : $ini - 10)) . " | ";
  }
  // Next button.
  $next = l(t('>>'), 'irl/manage/viewlogs/' . ($ini + 10));

  $content = _irl_logs_parse($logs->resp);
  $content .= $back . $next;
  return $content;
}

/**
 * Parse logs for actual viewing.
 */
function _irl_logs_parse($logs) {

  $view = '';

  $logTypes = array(
    'e' => t('E?'),
    'l' => t('L?'),
    'p' => t('P?'),
  );

  foreach ($logs as $log) {
    // print_r($log->idLog);
    // Time
    // ip
    // type
    // status
    // agent.
    $view .= '<div>';
    // Drupal short date.
    $view .= sprintf("<h2>%s</h2>", date('Y', $log->timestamp));
    $view .= sprintf("<p>IP %s</p>", $log->ip);
    $view .= '<p>' . t('(Your IP is encrypted with your key, if you want to see them you must use your irl app to view this logs.)') . '</p>';
    $view .= sprintf("<p>%s: %s</p>", t('Type'), $logTypes[$log->type]);
    $view .= sprintf("<p>%s: %s</p>", t('Status'), ($log->status == 0 ? t('Failed') : t('Success')));
    $view .= sprintf("<p>%s: %s</p>", t('Agent'), $log->agent);
    $view .= '<hr /></div>';
  }
  /*
   */
  return $view;
}

/**
 * Enable 2fa protection.
 */
function irl_2fa_enable() {

  global $user;

  $key = irl_key_request($user->uid);

  if (isset($key['err'])) {
    drupal_set_message(t('You do not have a key set, you must do this before you can enable 2FA protection', 'error'));
    drupal_goto('irl/manage/addkey');
  }

  // Already protected?
  $p = irl_user_protected_verify($user->uid);
  // print_r($p);
  if ($p['2faEnabled']) {
    drupal_set_message(t('Your account is already protected with 2FA via irl.'));
    drupal_goto('user/' . $user->uid . '/irl');
  }
  else {

    // The transaction opens here.
    $txn = db_transaction();
    try {

      $e = db_insert('irl_users')
        ->fields(array(
          'idUser' => $user->uid,
          'hasKey' => 'y',
          '2faEnabled' => 'y',
        ))
        ->execute();
    }
    catch (Exception $e) {
      // Something went wrong somewhere, so roll back now.
      $txn->rollback();
      $u = db_update('irl_users')
        ->fields(array(
          '2faEnabled' => 'y',
        ))
        ->condition('idUser', $user->uid, '=')
        ->execute();

      // Log the exception to watchdog.
      // watchdog_exception('type', $e);.
    }

    if ($e) {
      drupal_set_message(t('Your account is now protected with 2FA via irl.'));
    }
    else {
      drupal_set_message(t('Unable to set 2FA protection at this moment', 'error'));
    }
    drupal_goto('user/' . $user->uid . '/irl');
  }
}

/**
 * Disable 2fa protection.
 */
function irl_2fa_disable() {

  global $user;

  $u = db_update('irl_users')
    ->fields(array(
      '2faEnabled' => 'n',
    ))
    ->condition('idUser', $user->uid, '=')
    ->execute();

  drupal_set_message(t('Your account`s protection via 2FA was disabled.'));
  drupal_goto('user/' . $user->uid . '/irl');

}
