<?php

/**
 * @file
 * Install, update and uninstall functions for the irl module.
 */

/**
 * Implements hook_install().
 */
function irl_install() {
  drupal_set_message(t("Irl settings are available under !link",
        ['!link' => l(t('Administer > Users > irl'), 'admin/config/people/irl')]
  ));
}

/**
 * Implements hook_uninstall().
 */
function irl_uninstall() {
  variable_del('irl_server_path');
  variable_del('irl_server_token');
  variable_del('irl_challenge_max_time');
  variable_del('irl_allow_registration');
  variable_del('irl_require_email_for_registration');
  variable_del('irl_allow_global_offline_solve');
  variable_del('irl_server_client_id');
  variable_del('irl_server_client_ip');
}

/**
 * Implements hook_schema().
 */
function irl_schema() {

  $schema['irl_users'] = [
    'description' => 'Stores information about user`s irl usage.',
    'fields' => [
      'idIrl' => [
        'description' => 'Unique id for the relation',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'idUser' => [
        'description' => 'User uid',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'hasKey' => [
        'description' => 'If the user has a key set or not.',
        'type' => 'varchar',
        'length' => 2,
        'not null' => TRUE,
        'default' => 'n',
      ],
      '2faEnabled' => [
        'description' => 'If the user wants to enable 2fa to protect the account.',
        'type' => 'varchar',
        'length' => 2,
        'not null' => TRUE,
        'default' => 'n',
      ],
    ],
    'unique keys' => [
      'idUser' => ['idUser'],
    ],
    'indexes' => [
      '2faEnabled' => [
        '2faEnabled',
      ],
    ],
    'primary key' => [
      'idIrl',
    ],

  ];

  return $schema;
}
