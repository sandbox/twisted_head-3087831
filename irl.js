var irl_accessCode = 0;

/**
 * Request a new challenge and present a link to solve it
 */
function irl_successChallengeMe(data){

	data = JSON.parse(data);

	// @todo The first part of the path must be changed
	// it is not a 'regular' url, is the one that will be loaded
	// in the app
	path = '//irl/' +
		'?task=challengeAccept' +
		'&uid=' + irl_uid +
		'&url=' + irl_base_server_url +
		'&clientId=' + irl_clientId +
		'&secretCode=' + data.resp.accessCode +
		'&challenge=' + data.resp.challenge;

	console.log('Got a challenge: ' + data.resp.challenge);

	// @todo generate a clickable link
	jQuery('#irlLoginLink').val(path);

	irl_accessCode = data.resp.accessCode;

	// Generate QR Code
	var qrcode = new QRCode(document.getElementById("irlLoginQR"), {
		width : 250,
		height : 250
	});

	jQuery('#irlLoginQRWait').html('');

	qrcode.makeCode(path);

	// Add information for offline solutions
	jQuery('#irl_solution_uid').val(irl_uid);
	jQuery('#irl_solution_access_code').val(data.resp.accessCode);

}

/**
 * Sets up the page for a new login
 */
function irl_loginSetUp(uid){
	console.log('Creating a new challenge to allow user ' + uid + 'to lo gin');
	challengeMe();
}

/**
 * Confirms if the login challenge was passed
 */
function irl_autoConfirmChallengeLogin(){

	console.log('Set interval to check if the login was a success');

	var myVar = setInterval(_irl_autoConfirmChallengeLogin, 2000);

}

/**
 * Helper function to check the login process
 * This call is to Drupal, not the irl server for security reasons.
 * DO NOT CALL THIS FUNCTION
 */
function _irl_autoConfirmChallengeLogin() {

	console.log('Lets see if the challenge passed, accessCode: ' + irl_accessCode + ' uid: ' + irl_uid);

	jQuery.ajax({
		type: "POST",
		url: irl_base_url + '/?q=irl/check/' + irl_accessCode + '/' + irl_uid,
		data: {},
		success: irl_successChallengeConfirm,
		dataType: 'text'
	});

}

/**
 * Helper function to check the login process
 * DO NOT CALL THIS FUNCTION
 */
function irl_successChallengeConfirm(data){

	console.log('Challenge checked: ' + data);

	data = JSON.parse(data);

	if(data == 0){
		console.log('An error occured, this is probably bad and I should leave');
	}
	if(data == 1){
		console.log('Challenge passed!!!!');
		window.location.href = Drupal.settings.basePath;
	}

}

/**
 * Sets a timer in the page
 */
function irl_timerCountdown(div, start){

	console.log('Setting a timer in ' + div + ' starting from ' + start);

	setInterval(function(){
		console.log('Time remaining: ' + start);
		jQuery('#' + div).html(start);
		start = start - 1;
		if(start == 0){
			window.location.href = Drupal.settings.basePath;
		}
	}, 1000);
}

/**
 * Send offline solution.
 * @todo BUG! This should be done via php or maybe not.
 */
function irl_sendOfflineSolution(){

	// I will only send the response, if it works then the other checks
	// will find out and continue with the process as if working online.
	jQuery.ajax({
		type: "POST",
		url: irl_base_server_url,
		data: {
		'w': 'irl_challenge_accepted',
		'uid': irl_uid,
		'clientId': irl_clientId,
		'challenge': jQuery('#irl_offlineSolution').val()
		},
		dataType: 'text'
	});

	jQuery('#irl_submitedSolutionMsg').show();;

}

/**
 * Requests a new client account.
 */
function irl_client_register_new(){

	console.log('Request a new client account.');

	//var email = 'email': jQuery("input[name='irl_server_client_email']").val();

	if(!email){
		return false;
	}

	// Math.floor((Math.random() * 1000) + 1) + '@me.com'

	jQuery.ajax({
		type: "POST",
		url:'//localhost/irlKeyGen/',
		data: {
			'w': 'irls_client_register',
			'email': jQuery("input[name='irl_server_client_email']").val()
		},
		success: function(u){
			console.log(u)}
		,
		dataType: 'text'
	});

	return false;

}

function testMe(){
	console.log(':)');
	return false;
}
